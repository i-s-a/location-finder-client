import React from 'react';
import SearchNearest from './components/SearchNearest';
import SearchByFeatures from './components/SearchByFeatures';
import SearchByPlaceName from './components/SearchByPlaceName';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import NavBar from './components/NavBar';

const Routing = () => {
  return (
    <Switch>
      <Route exact path="/">
        <SearchNearest key="home" />
      </Route>
      <Route path="/SearchNearest">
        <SearchNearest key="searchnearest" />
      </Route>
      <Route path="/SearchByPlaceName">
        <SearchByPlaceName key="searchbyplacename" />
      </Route>
      <Route path="/SearchByFeatures">
        <SearchByFeatures key="searchbyfeatures" />
      </Route>
    </Switch>
  )
}

function App() {
  return (
    <BrowserRouter>
      <NavBar />
      <Routing />
    </BrowserRouter>
  )
}

export default App;
