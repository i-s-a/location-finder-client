import React from 'react'

function PlaceNameSearch(props) {
    return (
        <div>
            <div className='input-place-name'>
                <label htmlFor="inputPlaceName">Place Name</label>
                <input
                    type="text"
                    name="inputPlaceName"
                    id="inputPlaceName"
                    placeholder="e.g. British Museum"
                    value={props.inputPlaceName}
                    onChange={(e) => props.handleInputChange(e)} />
            </div>
            <div className="checkbox-container">
                <label className="checkbox-wrapper">
                    <input
                        className="filled-in similar-placename-checkbox"
                        type="checkbox"
                        name="inputIncludeSimilarNames"
                        id="inputIncludeSimilarNames"
                        checked={props.inputIncludeSimilarNames}
                        onChange={(e) => props.handleInputChange(e)} />
                    <span className="checkmark">Include similar names</span>
                </label>
            </div>
            <button onClick={(e) => props.retrievePlaceNamesClicked(e)} className='waves-effect waves-light btn-small'>Find by place name</button>
       </div>
    );
}

export default PlaceNameSearch;