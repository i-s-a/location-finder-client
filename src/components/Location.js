import React from "react";

function Location(props) {
    return (
        <tr>
            <th scope="row">{props.LocationId}</th>
            <td>{props.Name}</td>
            <td>{props.Latitude}</td>
            <td>{props.Longitude}</td>
            <td>{props.DistanceKm}</td>
        </tr>
    );
}

export default Location;