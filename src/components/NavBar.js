import React from 'react'
import { Link, useHistory } from 'react-router-dom'

const NavBar = () => {
    const renderList = () => {
        return [
            <ul key="navlinks" id="nav-mobile" className="left hide-on-med-and-down">
                <li key="searchnearest"><Link key="searchnearestlink" to="./SearchNearest">Search Nearest Locations</Link></li>
                <li key="searchbyplacename"><Link key="searchplacenamelink" to="./SearchByPlaceName">Search By Place Name</Link></li>
                <li key="searchbyfeatures"><Link key="searchfeatureslink" to="./SearchByFeatures">Search By Features</Link></li>
            </ul>
        ]
    }

    return (
        <nav>
            <div className="nav-wrapper">
                <Link to="SearchNearest" className="brand-logo right">Location Finder</Link>
                    {renderList()}
            </div>
        </nav>
    )
}

export default NavBar;