import React from "react";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";

function LocationMapDisplay(props) {

    const [activeLocation, setActiveLocation] = React.useState(null);

    return (
        // <Map center={[29.98333, 31.13333]} zoom={15}>
        <Map center={[props.centreLatitude, props.centreLongitude]} zoom={12}>
            {props.locations.map(loc => (
                <Marker
                    key={loc.id}
                    position={[loc.Latitude, loc.Longitude]}
                    onClick={() => {
                        setActiveLocation(loc);
                    }}
                />
            ))}
            
            <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            />

            {activeLocation && (
                <Popup
                    position={[activeLocation.Latitude, activeLocation.Longitude]}
                    onClose={() => {setActiveLocation(null)}}>

                    <div>
                        <h6>{activeLocation.Name}</h6>
                        <p>Distance: {activeLocation.DistanceKm} Km</p>
                    </div>
                </Popup>
            )}
        </Map>
    );
}

export default LocationMapDisplay;