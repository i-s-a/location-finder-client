import React from "react";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";

function PlaceMapDisplay(props) {

    const [activePlace, setActivePlace] = React.useState(null);

    return (
        // <Map center={[45.4, -75.7]} zoom={12}>
        <Map center={[props.centreLatitude, props.centreLongitude]} zoom={6}>
            {props.places.map(place => (
                <Marker
                    key={place.id}
                    position={[place.Latitude, place.Longitude]}
                    onClick={() => {
                        setActivePlace(place);
                    }}
                />
            ))}
            <TileLayer
                url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            />

            {activePlace && (
                <Popup
                    position={[activePlace.Latitude, activePlace.Longitude]}
                    onClose={() => { setActivePlace(null)}}>

                    <div>
                        <h6>{activePlace.Name}</h6>
                    </div>
                </Popup>
                
            )}
        </Map>
    );
}

export default PlaceMapDisplay;