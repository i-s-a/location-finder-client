import React from 'react'

function FeatureSearch(props) {
    return (
        <>
            <div className='features-list-box-wrapper'>
                <label>Feature Codes</label>
                <div className='input-field col s12'>
                    <select className="browser-default" id="inputFeatureCode" name="inputFeatureCode" size="6" onChange={(e) => props.handleCodeChange(e)}>
                        {props.featureCodes.map(f =>
                            <option key={f.FeatureCodeId} value={f.FeatureCodeId}>{f.FeatureCodeName}</option>)}
                    </select>
                </div>
            </div>
            <button
                onClick={(e) => props.retrievePlaceNamesByFeaturesClicked(e)}
                className='waves-effect waves-light btn-small'>Find by feature code</button>
        </>
    );
}

export default FeatureSearch;