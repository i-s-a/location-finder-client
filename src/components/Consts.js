export const LOCATION_FINDER_SERVER_URL = 'https://location-finder-100.herokuapp.com/api/v1/';
export const DEFAULT_LOCATION_LATITUDE = 51.509865;
export const DEFAULT_LOCATION_LONGITUDE = -0.118092;
