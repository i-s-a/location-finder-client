import React, { Component } from 'react'
import {LocationsClientConnection} from "./LocationsClientConnection";
import "./Main.css"
import ViewType from "./ViewType";
import CountryFilter from "./CountryFilter";
import FeatureSearch from "./FeatureSearch";
import PlaceList from "./PlaceList";
import PlaceMapDisplay from "./PlaceMapDisplay";
import {LOCATION_FINDER_SERVER_URL, DEFAULT_LOCATION_LATITUDE, DEFAULT_LOCATION_LONGITUDE} from "./Consts";

class SearchByFeatures extends Component {
    #clientConnection = new LocationsClientConnection(LOCATION_FINDER_SERVER_URL);

    //London latitude and longitude is the default location
    #DEFAULT_LATITUDE = DEFAULT_LOCATION_LATITUDE;
    #DEFAULT_LONGITUDE = DEFAULT_LOCATION_LONGITUDE;

    MaxItemsPerPage = 10;

    constructor(props) {
        super(props);

        this.state = {
            places: [],
            inputCountryCode: 'any',
            inputFeatureCode: '',
            featureCodes: [],
            countryCodes: [],
            pageLinks: [],
            totalCount: 0,
            currentPage: 0,
            prevPageLink: '',
            nextPageLink: '',
            centreLatitude: this.#DEFAULT_LATITUDE,
            centreLongitude: this.#DEFAULT_LONGITUDE,
            viewType: "table",
        }
    }

    async componentDidMount() {
        try {
            
            this.setCurrentLocation();

            const copyOfFeatureCodes = await this.#clientConnection.getFeatureCodes();

            const copyOfCountryCodes = await this.#clientConnection.getCountryCodes();

            this.setState({
                featureCodes: copyOfFeatureCodes,
                countryCodes: copyOfCountryCodes,
            });
        }
        catch (error) {
            console.log(error);
            alert(error);
        }
    }

    setCurrentLocation = () => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(this.saveCurrentLocation, this.errorCurrentLocation);
        }
        else {
            alert("Geolocation is not supported by this browser");
        }
    }

    saveCurrentLocation = (position) => {
        const latitude = this.roundNumber(position.coords.latitude, 6);
        const longitude = this.roundNumber(position.coords.longitude, 6);
        this.setState({
            inputLatitude: latitude,
            inputLongitude: longitude
        });
    }

    errorCurrentLocation = () => {
        alert("Failed to retrieve current location");
        //Can't get the current location, default the location to London
        const latitude = this.#DEFAULT_LATITUDE;
        const longitude = this.#DEFAULT_LONGITUDE;
        this.setState({
            inputLatitude: latitude,
            inputLongitude: longitude
        });
    }

    roundNumber = (num, decimalPlaces) => {
        return Number(Math.round(num + 'e' + decimalPlaces) + 'e-' + decimalPlaces);
    }

    RetrievePlaceNamesByFeatureCode = async (e) => {

        e.preventDefault();

        try {

            const featureCodeId = this.state.inputFeatureCode;
            let countryCodeId = null;
            if (this.state.inputCountryCode !== "any")
                countryCodeId = this.state.inputCountryCode;

            const copyOfPlaces = await this.#clientConnection.getPlaceNamesByFeature(featureCodeId, 
                countryCodeId, 0, this.MaxItemsPerPage);            

            const count = await this.#clientConnection.getPlaceNamesByFeatureCount(featureCodeId, countryCodeId);

            let pageLinks = [];
            const numPages = Math.ceil(count / this.MaxItemsPerPage);

            let url = this.#clientConnection.locationsFeaturesUrl(featureCodeId);
            if (this.state.inputCountryCode !== "any")
                url = this.#clientConnection.locationsFeaturesAndCountryUrl(featureCodeId, countryCodeId);

            for (let i = 0; i < numPages; ++i) {
                const offset = i * this.MaxItemsPerPage;
                const pageUrl = url + `?offset=${offset}&limit=${this.MaxItemsPerPage}`;
                pageLinks.push(pageUrl);
            }

            let nextPage = '';
            if (pageLinks.length > 1) {
                nextPage = pageLinks[1];
            }
            
            let prevPage = '';

            const centreCoords = this.calculateCentreCoordinates(copyOfPlaces);

            this.setState({
                places: copyOfPlaces,
                pageLinks: pageLinks,
                totalCount: count,
                currentPage: 0,
                prevPageLink: prevPage,
                nextPageLink: nextPage,
                centreLatitude: centreCoords.latitude,
                centreLongitude: centreCoords.longitude,
            });
        }
        catch(error) {
            console.log(error.name);
            console.log(error.message);
            alert(error.name + '\n' + error.message);
        }
    }

    handleCodeChange = (e) => {

        e.preventDefault();

        const target = e.target;
        const name = target.name;
        const value = target.value;
 
        this.setState({
            [name]: value
        });
    }

    pageLinkClicked = async (e, indexClicked, href) => {

        e.preventDefault();

        try {
            if (this.state.places !== null) {

                console.log(href);

                const copyOfPlaces = await this.#clientConnection.retrievePlaceNames(href);

                let nextPage = '';
                if (indexClicked < this.state.pageLinks.length - 1) {
                    nextPage = this.state.pageLinks[indexClicked + 1];
                }
                
                let prevPage = '';
                if (indexClicked > 0) {
                    prevPage = this.state.pageLinks[indexClicked - 1];
                }

                const centreCoords = this.calculateCentreCoordinates(copyOfPlaces);

                this.setState({
                    places: copyOfPlaces,
                    currentPage: indexClicked,
                    prevPageLink: prevPage,
                    nextPageLink: nextPage,                    
                    centreLatitude: centreCoords.latitude,
                    centreLongitude: centreCoords.longitude,
                });                
            }
        }
        catch(error) {
            console.log(error.name);
            console.log(error.message);
            alert(error.name + '\n' + error.message);
        }

        return false;
    }

    calculateCentreCoordinates = (coordinatesArray) => {

        //TODO: For now just return the first item in the array as the centre coordinate.
        //Need to change this to something better!!
        if (coordinatesArray.length > 0) {
            const centreLatitude = coordinatesArray[0].Latitude;
            const centreLongitude = coordinatesArray[0].Longitude;
            
            let coords = {
                latitude: centreLatitude,
                longitude: centreLongitude
            };
    
            return coords;
        }
        else {
            //TODO: What should we return for the centre coords if the array is empty?
            //For now return the default latitude and longitude
            let coords = {
                latitude: this.#DEFAULT_LATITUDE,
                longitude: this.#DEFAULT_LONGITUDE,
            };

            return coords;
        }
    }

    switchViewType = (viewTypeToDisplay) => {
        this.setState({
            viewType: viewTypeToDisplay
        });
    }

    render() {
        return (
            <div className="wrapper">
                <div className="box sidebar">
                    <ViewType onClick={this.switchViewType} />

                    <CountryFilter countryCodes={this.state.countryCodes} onChange={this.handleCodeChange} />

                    <FeatureSearch
                        featureCodes={this.state.featureCodes}
                        handleCodeChange={this.handleCodeChange} 
                        retrievePlaceNamesByFeaturesClicked={this.RetrievePlaceNamesByFeatureCode} />
                </div>
                <div className="box content">
                    {this.state.viewType === 'table' ?
                        <PlaceList places={this.state.places} />
                        :
                        <PlaceMapDisplay centreLatitude={this.state.centreLatitude} centreLongitude={this.state.centreLongitude} places={this.state.places} />
                    }
                    <ul>
                        {this.state.currentPage > 0
                            ? <li className="page-item"><a className="previous" onClick={(e) => this.pageLinkClicked(e, this.state.currentPage - 1, this.state.prevPageLink)} href={this.state.prevPageLink}>Previous</a></li>
                            : <li className="page-item-disabled">Previous</li>}

                        {this.state.pageLinks.map((p, i) => {
                            if (this.state.currentPage === i) {
                                return <li key={i} className="page-item"><div className="page-link-selected" onClick={(e) => this.pageLinkClicked(e, i, p)} href={p}>{i+1}</div></li>
                            }
                            else {
                                return <li key={i} className="page-item"><div className="page-link" onClick={(e) => this.pageLinkClicked(e, i, p)} href={p}>{i+1}</div></li>
                            }
                        })
                        }

                        {this.state.currentPage < this.state.pageLinks.length - 1
                            ? <li className="page-item"><a className="next" onClick={(e) => this.pageLinkClicked(e, this.state.currentPage + 1, this.state.nextPageLink)} href={this.state.nextPageLink}>Next</a></li>
                            : <li className="page-item-disabled">Next</li>}                        
                    </ul>
                </div>
            </div>
        );
    }    
}

export default SearchByFeatures;