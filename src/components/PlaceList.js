import React from "react";
import Place from './Place'

function PlaceList(props) {
    return (
        <table className="table table-hover table-bordered bg-light">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Latitude</th>
                    <th scope="col">Longitude</th>
                </tr>
            </thead>            
            <tbody>
                {props.places && props.places.length
                ? props.places.map(p => 
                    <Place key={p.id} LocationId={p.LocationId} Name={p.Name} Latitude={p.Latitude} Longitude={p.Longitude} />)
                : <Place key="0" LocationId="No Results" Name="" Latitude="" Longitude="" />}
            </tbody>
        </table>
    )
}

export default PlaceList;