import React, { Component } from 'react'
import {LocationsClientConnection} from "./LocationsClientConnection";
import "./Main.css"
import ViewType from "./ViewType";
import WithinBoundarySearch from "./WithinBoundarySearch";
import LocationList from "./LocationList";
import LocationMapDisplay from "./LocationMapDisplay"
import {LOCATION_FINDER_SERVER_URL, DEFAULT_LOCATION_LATITUDE, DEFAULT_LOCATION_LONGITUDE} from "./Consts";

class SearchNearest extends Component {
    #clientConnection = new LocationsClientConnection(LOCATION_FINDER_SERVER_URL);

    //London latitude and longitude is the default location
    #DEFAULT_LATITUDE = DEFAULT_LOCATION_LATITUDE;
    #DEFAULT_LONGITUDE = DEFAULT_LOCATION_LONGITUDE;

    MaxItemsPerPage = 10;

    constructor(props) {
        super(props);

        this.state = {
            locations: [],
            inputLatitude: this.#DEFAULT_LATITUDE,
            inputLongitude: this.#DEFAULT_LONGITUDE,
            inputDistance: 1,
            pageLinks: [],
            totalCount: 0,
            currentPage: 0,
            prevPageLink: '',
            nextPageLink: '',
            centreLatitude: this.#DEFAULT_LATITUDE,
            centreLongitude: this.#DEFAULT_LONGITUDE,
            viewType: "table",
            featureCodes: [],
            inputFeatureCode: '',
        }
    }

    async componentDidMount() {
        try {
            //alert("Nearest Mounted");
            this.setCurrentLocation();

            const copyOfFeatureCodes = await this.#clientConnection.getFeatureCodes();

            this.setState({
                featureCodes: copyOfFeatureCodes,
            });
        }
        catch (error) {
            console.log(error);
            alert(error);
        }
    }

    setCurrentLocation = () => {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(this.saveCurrentLocation, this.errorCurrentLocation);
        }
        else {
            alert("Geolocation is not supported by this browser");
        }
    }

    saveCurrentLocation = (position) => {
        const latitude = this.roundNumber(position.coords.latitude, 6);
        const longitude = this.roundNumber(position.coords.longitude, 6);
        this.setState({
            inputLatitude: latitude,
            inputLongitude: longitude,
            centreLatitude:latitude,
            centreLongitude:longitude,
        });
    }

    errorCurrentLocation = () => {
        alert("Failed to retrieve current location");
        //Can't get the current location, default the location to London
        const latitude = this.#DEFAULT_LATITUDE;
        const longitude = this.#DEFAULT_LONGITUDE;
        this.setState({
            inputLatitude: latitude,
            inputLongitude: longitude,
            centreLatitude:latitude,
            centreLongitude:longitude,
        });
    }

    roundNumber = (num, decimalPlaces) => {
        return Number(Math.round(num + 'e' + decimalPlaces) + 'e-' + decimalPlaces);
    }

    handleInputChange = (e) => {

        e.preventDefault();

        const target = e.target;
        const name = target.name;
        const value = target.value;

        this.setState({
            [name]: value
        });
    }

    RetrieveLocationsWithinBoundary = async (e) => {

        e.preventDefault();

        try {
            const featureCodeId = this.state.inputFeatureCode;

            const copyOfLocations = await this.#clientConnection.getLocationsWithinBoundary(
                this.state.inputLatitude, this.state.inputLongitude, this.state.inputDistance, featureCodeId, 0,
                this.MaxItemsPerPage
                );

            const count = await this.#clientConnection.getLocationsWithinBoundaryCount(
                this.state.inputLatitude, this.state.inputLongitude, this.state.inputDistance, featureCodeId
                );

            let pageLinks = [];
            const numPages = Math.ceil(count / this.MaxItemsPerPage);

            const url = this.#clientConnection.locationsWithinBoundaryUrl(
                this.state.inputLatitude, this.state.inputLongitude, this.state.inputDistance, featureCodeId
                );

            for (let i = 0; i < numPages; ++i) {
                const offset = i * this.MaxItemsPerPage;
                const pageUrl = url + `&offset=${offset}&limit=${this.MaxItemsPerPage}`;
                pageLinks.push(pageUrl);
            }

            let nextPage = '';
            if (pageLinks.length > 1) {
                nextPage = pageLinks[1];
            }
            
            let prevPage = '';

            const centreLat = this.state.inputLatitude;
            const centreLon = this.state.inputLongitude;

            this.setState({
                locations: copyOfLocations,
                places: null,
                pageLinks: pageLinks,
                totalCount: count,
                currentPage: 0,
                prevPageLink: prevPage,
                nextPageLink: nextPage,
                centreLatitude: centreLat,
                centreLongitude: centreLon,
            });
        }
        catch(error) {
            console.log(error.name);
            console.log(error.message);
            alert(error.name + '\n' + error.message);
        }
    }

    pageLinkClicked = async (e, indexClicked, href) => {

        e.preventDefault();

        try {
            if (this.state.locations !== null) {

                const copyOfLocations = await this.#clientConnection.retrieveLocationWithinBoundaryLink(href);

                let nextPage = '';
                if (indexClicked < this.state.pageLinks.length - 1) {
                    nextPage = this.state.pageLinks[indexClicked + 1];
                }
                
                let prevPage = '';
                if (indexClicked > 0) {
                    prevPage = this.state.pageLinks[indexClicked - 1];
                }

                const centreLat = this.state.inputLatitude;
                const centreLon = this.state.inputLongitude;    

                this.setState({
                    locations: copyOfLocations,
                    currentPage: indexClicked,
                    prevPageLink: prevPage,
                    nextPageLink: nextPage,
                    centreLatitude: centreLat,
                    centreLongitude: centreLon,
                });
            }
        }
        catch(error) {
            console.log(error.name);
            console.log(error.message);
            alert(error.name + '\n' + error.message);
        }

        return false;
    }

    calculateCentreCoordinates = (coordinatesArray) => {

        //TODO: For now just return the first item in the array as the centre coordinate.
        //Need to change this to something better!!
        if (coordinatesArray.length > 0) {
            const centreLatitude = coordinatesArray[0].Latitude;
            const centreLongitude = coordinatesArray[0].Longitude;
            
            let coords = {
                latitude: centreLatitude,
                longitude: centreLongitude
            };
    
            return coords;
        }
        else {
            //TODO: What should we return for the centre coords if the array is empty?
            //For now return the default latitude and longitude
            let coords = {
                latitude: this.#DEFAULT_LATITUDE,
                longitude: this.#DEFAULT_LONGITUDE,
            };

            return coords;
        }
    }

    switchViewType = (viewTypeToDisplay) => {
        this.setState({
            viewType: viewTypeToDisplay
        });
    }

    render() { 
        return (
            <div className="wrapper">
                <div className="box sidebar">
                    <ViewType onClick={this.switchViewType} />

                    <WithinBoundarySearch
                        inputLatitude={this.state.inputLatitude}
                        inputLongitude={this.state.inputLongitude}
                        inputDistance={this.state.inputDistance}
                        handleInputChange={this.handleInputChange}
                        retrieveLocationsWithinBoundaryClicked={this.RetrieveLocationsWithinBoundary} />
                </div>
                <div className="box content">
                    {this.state.viewType === 'table' ?
                        <LocationList locations={this.state.locations} />
                        :
                        <LocationMapDisplay centreLatitude={this.state.centreLatitude} centreLongitude={this.state.centreLongitude} locations={this.state.locations} />
                    }
                    <ul>
                        {this.state.currentPage > 0
                            ? <li className="page-item"><a className="previous" onClick={(e) => this.pageLinkClicked(e, this.state.currentPage - 1, this.state.prevPageLink)} href={this.state.prevPageLink}>Previous</a></li>
                            : <li className="page-item-disabled">Previous</li>}

                        {this.state.pageLinks.map((p, i) => {
                            if (this.state.currentPage === i) {
                                return <li key={i} className="page-item"><div className="page-link-selected" onClick={(e) => this.pageLinkClicked(e, i, p)} href={p}>{i+1}</div></li>
                            }
                            else {
                                return <li key={i} className="page-item"><div className="page-link" onClick={(e) => this.pageLinkClicked(e, i, p)} href={p}>{i+1}</div></li>
                            }
                        })
                        }

                        {this.state.currentPage < this.state.pageLinks.length - 1
                            ? <li className="page-item"><a className="next" onClick={(e) => this.pageLinkClicked(e, this.state.currentPage + 1, this.state.nextPageLink)} href={this.state.nextPageLink}>Next</a></li>
                            : <li className="page-item-disabled">Next</li>}                        
                    </ul>
                </div>
            </div>
        );
    }
}

export default SearchNearest;