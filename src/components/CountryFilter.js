import React from 'react'

function CountryFilter(props) {
    console.log(props.countryCodes.length);
    return (
        <div className='country-list-box-wrapper'>
            <label>Country Codes</label>
            <div className='input-field col s12'>
                <select className="browser-default" id="inputCountryCode" name="inputCountryCode" onChange={(e) => props.onChange(e)}>
                    <option key="any" value="any">Any</option>
                    {props.countryCodes.map(c =>
                        <option key={c.CountryCodeId} value={c.CountryCodeId}>{c.CountryCodeName}</option>)}
                </select>
            </div>
        </div>
    )
}

export default CountryFilter;