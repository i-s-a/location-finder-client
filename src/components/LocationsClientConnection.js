import axios from "axios";

function ClientConnectionException(name, message) {
    this.name = name;
    this.message = message;
}

class LocationsClientConnection {
    #baseUrl;

    constructor(baseUrl) {
        this.#baseUrl = baseUrl;
    }

    get baseUrl() {
        return this.#baseUrl;
    }

    locationsNameUrl(name, includeSimilarNames) {
        return this.#baseUrl + `locations/names/${name}?similar=${includeSimilarNames.toString()}`;
    }

    locationsNameAndCountryUrl(name, includeSimilarNames, countryCodeId) {
        return this.#baseUrl + `locations/names/${name}/countries/${countryCodeId}?similar=${includeSimilarNames.toString()}`;
    }

    locationsWithinBoundaryUrl(latitude, longitude, distanceKm, featureCodeId) {

        if (featureCodeId === undefined || featureCodeId === null) {
            return this.#baseUrl + `locations?lat=${latitude}&lon=${longitude}&distance=${distanceKm}&sort=name`;
        }
        else {
            return this.#baseUrl + `locations?lat=${latitude}&lon=${longitude}&distance=${distanceKm}&featureid=${featureCodeId}&sort=name`;            
        }
    }

    locationsFeaturesUrl(featureCodeId) {
        return this.#baseUrl + `locations/features/${featureCodeId}`;
    }

    locationsFeaturesAndCountryUrl(featureCodeId, countryCodeId) {
        return this.#baseUrl + `locations/features/${featureCodeId}/countries/${countryCodeId}`;
    }

    featureCodesUrl() {
        return this.#baseUrl + 'codes/features';
    }

    featureCodeByIdUrl(id) {
        return this.#baseUrl + `codes/features/${id}`
    }

    countryCodesUrl() {
        return this.#baseUrl + 'codes/countries';
    }

    countryCodeByIdUrl(id) {
        return this.#baseUrl + `codes/countries/${id}`;
    }

    async retrievePlaceNames(href) {
        try {
            const response = await axios.get(href);
            const returnedPlaceNames = response.data.map(p => {
                return {
                    id: p.id,
                    LocationId: p.id,
                    Name: p.name,
                    Latitude: p.coordinates.latitude,
                    Longitude: p.coordinates.longitude
                }
            });
    
            return returnedPlaceNames;
        }
        catch (error) {
            throw new ClientConnectionException('Failed to retrieve place names', error.message);
        }
    }

    async getPlaceNames(name, includeSimilarNames, countryCodeId, offset, limit) {
        try {

            let url = '';

            if (countryCodeId === undefined || countryCodeId === null)
                url = this.locationsNameUrl(name, includeSimilarNames);
            else
                url = this.locationsNameAndCountryUrl(name, includeSimilarNames, countryCodeId);

            if (typeof offset !== "undefined" && typeof offset === "number" &&
                typeof limit !== "undefined" && typeof limit === "number") {
                url = url + `&offset=${offset}&limit=${limit}`;
            }
            else if (typeof limit !== "undefined" && typeof limit === "number") {
                url = url + `&limit=${limit}`;
            }

            return await this.retrievePlaceNames(url);
        }
        catch(error) {
            throw error;
        }
    }

    async getPlaceNamesCount(name, includeSimilarNames, countryCodeId) {
        try {
            
            let url = '';

            if (countryCodeId === undefined || countryCodeId === null)
                url = this.locationsNameUrl(name, includeSimilarNames);
            else
                url = this.locationsNameAndCountryUrl(name, includeSimilarNames, countryCodeId);

            const countResponse = await axios.head(url);
            const count = countResponse.headers['x-total-count'];

            return count;
        }
        catch (error) {
            throw new ClientConnectionException('Failed to retrieve count of place names', error.message);
        }
    }

    async retrieveLocationWithinBoundaryLink(url) {
        try {

            const response = await axios.get(url);
            const returnedLocations = response.data.map(loc => {
                return {
                    id: loc.id,
                    LocationId: loc.id,
                    Name: loc.name,
                    Latitude: loc.coordinates.latitude,
                    Longitude: loc.coordinates.longitude,
                    DistanceKm: loc.distanceKm
                }
            });

            return returnedLocations;
        }
        catch (error) {
            throw new ClientConnectionException('Failed to retrieve locations', error.message);
        }
    }

    async getLocationsWithinBoundary(latitude, longitude, distanceKm, featureCodeId, offset, limit) {
        try {

            let url = this.locationsWithinBoundaryUrl(latitude, longitude, distanceKm, featureCodeId);            

            if (typeof offset !== "undefined" && typeof offset === "number" &&
                typeof limit !== "undefined" && typeof limit === "number") {
                url = url + `&offset=${offset}&limit=${limit}`;
            }
            else if (typeof limit !== "undefined" && typeof limit === "number") {
                url = url + `&limit=${limit}`;
            }

            return await this.retrieveLocationWithinBoundaryLink(url);
        }
        catch (error) {
            throw error;
        }
    }

    async getLocationsWithinBoundaryCount(latitude, longitude, distanceKm, featureCodeId) {
        try {

            let url = this.locationsWithinBoundaryUrl(latitude, longitude, distanceKm, featureCodeId);

            const countResponse = await axios.head(url);
            const count = countResponse.headers['x-total-count'];

            return count;
        }
        catch (error) {
            throw new ClientConnectionException('Failed to retrieve count of locations', error.message);
        }
    }

    async getPlaceNamesByFeature(featureCodeId, countryCodeId, offset, limit) {
        try {

            let url = '';

            if (countryCodeId === undefined || countryCodeId === null)
                url = this.locationsFeaturesUrl(featureCodeId);
            else
                url = this.locationsFeaturesAndCountryUrl(featureCodeId, countryCodeId);

            if (typeof offset !== "undefined" && typeof offset === "number" &&
                typeof limit !== "undefined" && typeof limit === "number") {
                url = url + `?offset=${offset}&limit=${limit}`;
            }
            else if (typeof limit !== "undefined" && typeof limit === "number") {
                url = url + `?limit=${limit}`;
            }

            return await this.retrievePlaceNames(url);
        }
        catch(error) {
            throw error;
        }
    }

    async getPlaceNamesByFeatureCount(featureCodeId, countryCodeId) {
        try {
            let url = '';

            if (countryCodeId === undefined || countryCodeId === null)
                url = this.locationsFeaturesUrl(featureCodeId);
            else
                url = this.locationsFeaturesAndCountryUrl(featureCodeId, countryCodeId);

            const countResponse = await axios.head(url);
            const count = countResponse.headers['x-total-count'];

            return count;
        }
        catch (error) {
            throw new ClientConnectionException('Failed to retrieve count of place names', error.message);
        }
    }

    async retrieveFeaturecodesLink(url) {
        try {
            const response = await axios.get(url);
            const returnedFeatureCodes = response.data.map(featureCode => {
                return {
                    FeatureCodeId: featureCode.id,
                    FeatureCodeName: featureCode.name,
                }
            });

            return returnedFeatureCodes;
        }
        catch (error) {
            throw new ClientConnectionException('Failed to retrieve feature codes', error.message);
        }
    }

    async getFeatureCodes() {
        try {
            const url = this.featureCodesUrl();
            return await this.retrieveFeaturecodesLink(url);
        }
        catch (error) {
            throw error;
        }
    }

    async retrieveCountryCodesLink(url) {
        try {
            const response = await axios.get(url);
            const returnedCountryCodes = response.data.map(countryCode => {
                return {
                    CountryCodeId: countryCode.id,
                    CountryCodeName: countryCode.name,
                    CountryCodeAlpha3Code: countryCode.alpha3Code,
                    CountryCodeNumericCode: countryCode.numericCode,
                }
            });

            return returnedCountryCodes;
        }
        catch (error) {
            throw new ClientConnectionException('Failed to retrieve country codes', error.message);
        }
    }

    async getCountryCodes() {
        try {
            const url = this.countryCodesUrl();
            return await this.retrieveCountryCodesLink(url);
        }
        catch (error) {
            throw error;
        }
    }    
}

export {
    LocationsClientConnection,
    ClientConnectionException,
}