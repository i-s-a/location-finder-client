import React from "react";
import Location from './Location'

function LocationList(props) {
    return (
        <table className="table table-hover table-bordered bg-light">
            <thead>
                <tr>
                    <th scope="col">#</th>
                    <th scope="col">Name</th>
                    <th scope="col">Latitude</th>
                    <th scope="col">Longitude</th>
                    <th scope="col">Distance Km</th>
                </tr>
            </thead>
            <tbody>
                {props.locations && props.locations.length
                ? props.locations.map(p => 
                    <Location key={p.id} LocationId={p.LocationId} Name={p.Name} Latitude={p.Latitude} Longitude={p.Longitude} DistanceKm={p.DistanceKm} />)
                : <Location key="0" LocationId="No Results" Name="" Latitude="" Longitude="" DistanceKm="" />}
            </tbody>
        </table>
    )
}

export default LocationList;