import React from "react";

function Place(props) {
    return (
        <tr>
            <th scope="row">{props.LocationId}</th>
            <td>{props.Name}</td>
            <td>{props.Latitude}</td>
            <td>{props.Longitude}</td>
        </tr>
    );
}

export default Place