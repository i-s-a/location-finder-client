import React from 'react'

function WithinBoundarySearch(props) {
    return (
        <div>
            <div className='input-lat'>
                <label htmlFor="inputLatitude">Latitude</label>
                <input
                    type="text"
                    name="inputLatitude"
                    id="inputLatitude"
                    value={props.inputLatitude}
                    onChange={(e) => props.handleInputChange(e)} />
            </div>
            <div className='input-lon'>
                <label htmlFor="inputLongitude">Longitude</label>
                <input
                    type="text"
                    name="inputLongitude"
                    id="inputLongitude"
                    value={props.inputLongitude}
                    onChange={(e) => props.handleInputChange(e)} />
            </div>
            <div className='input-dist'>
                <label htmlFor="inputDistance">Distance Km</label>
                <input
                    type="text"
                    className="form-control"
                    name="inputDistance"
                    id="inputDistance"
                    value={props.inputDistance}
                    onChange={(e) => props.handleInputChange(e)} />
            </div>
            <button onClick={(e) => props.retrieveLocationsWithinBoundaryClicked(e)} className='waves-effect waves-light btn-small'>Find within radius</button>
        </div>
    );
}

export default WithinBoundarySearch;