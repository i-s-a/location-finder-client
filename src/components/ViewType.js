import React from 'react'

function ViewType(props) {
    return (
        <>
            <button className='waves-effect waves-light btn-small table' onClick={() => props.onClick('table')}>Table View</button>
            <button className='waves-effect waves-light btn-small map' onClick={() => props.onClick('map')}>Map View</button>
        </>
    );
}

export default ViewType;